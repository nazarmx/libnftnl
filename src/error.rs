use std::os::raw::c_int;

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub struct Error {
    pub code: c_int,
}
impl From<c_int> for Error {
    fn from(code: c_int) -> Self {
        Self { code }
    }
}
impl Default for Error {
    fn default() -> Self {
        Self { code: -1 }
    }
}
