use libnftnl_sys::*;
use error::Error;

#[derive(Debug)]
pub struct Batch {
    batch: *mut nftnl_batch,
}

impl Batch {
    pub fn new(pg_size: u32, pg_overrun_size: u32) -> Result<Self, Error> {
        unsafe {
            let batch: *mut nftnl_batch = nftnl_batch_alloc(pg_size, pg_overrun_size);
            if batch.is_null() {
                Err(Error::default())
            } else {
                Ok(Self { batch })
            }
        }
    }

    pub fn is_supported() -> Result<bool, Error> {
        let code = unsafe { nftnl_batch_is_supported() };
        match code {
            1 => Ok(true),
            0 => Ok(false),
            _ => Err(code.into()),
        }
    }
}
impl Drop for Batch {
    fn drop(&mut self) {
        unsafe {
            nftnl_batch_free(self.batch);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn batch_new_works() {
        Batch::new(4096, 0).unwrap();
    }

    #[test]
    fn batch_is_supported_works() {
        Batch::is_supported().unwrap();
    }
}
