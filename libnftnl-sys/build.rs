//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.

extern crate bindgen;
extern crate pkg_config;

use bindgen::builder;
use std::env;
use std::path::PathBuf;

fn main() {
    const NFTNL_VERSION: &'static str = "1.1.1";
    const NFTNL_PKG_CONFIG: &'static str = "libnftnl";
    const NFTNL_REGEX: &'static str = "^nftnl_.+$";
    const NFTNL_FLAGS_REGEX: &'static str = "^nftnl_.+_flags$";
    const NFTNL_NOT_FLAGS_REGEX: &'static str = "^nftnl_.+[^_][^f][^l][^a][^g][^s]$";

    pkg_config::Config::new()
        .atleast_version(NFTNL_VERSION)
        .probe(NFTNL_PKG_CONFIG)
        .unwrap();

    let bindings = builder()
        .header("wrapper.h")
        .opaque_type(NFTNL_REGEX)
        .opaque_type("_IO_FILE")
        .blacklist_type("iovec")
        .whitelist_type(NFTNL_REGEX)
        .whitelist_type("^_bindgen.*")
        .whitelist_var(NFTNL_REGEX)
        .whitelist_function(NFTNL_REGEX)
        .bitfield_enum(NFTNL_FLAGS_REGEX)
        .constified_enum_module(NFTNL_NOT_FLAGS_REGEX)
        .rustified_enum("^_bindgen.*")
        .prepend_enum_name(false)
        .generate()
        .unwrap();
    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings.write_to_file(out_path.join("bindings.rs")).unwrap();
}
